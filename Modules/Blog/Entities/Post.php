<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'blog__posts';
    protected $fillable = [
        'category_id',
        'title',
        'url',
        'description',
        'content',
        'publication_at',
    ];
}
