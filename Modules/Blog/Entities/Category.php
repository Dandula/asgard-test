<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'blog__categories';
    protected $fillable = [
        'title',
        'url',
    ];
}
