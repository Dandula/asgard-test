<?php

namespace Modules\Blog\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Blog\Composers\AllPostsComposer;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Blog\Events\Handlers\RegisterBlogSidebar;

class BlogServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterBlogSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('categories', array_dot(trans('blog::categories')));
            $event->load('posts', array_dot(trans('blog::posts')));
            // append translations


        });

        view()->composer('blog.index', AllPostsComposer::class);
    }

    public function boot()
    {
        $this->publishConfig('blog', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Blog\Repositories\CategoryRepository',
            function () {
                $repository = new \Modules\Blog\Repositories\Eloquent\EloquentCategoryRepository(new \Modules\Blog\Entities\Category());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Blog\Repositories\Cache\CacheCategoryDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Blog\Repositories\PostRepository',
            function () {
                $repository = new \Modules\Blog\Repositories\Eloquent\EloquentPostRepository(new \Modules\Blog\Entities\Post());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Blog\Repositories\Cache\CachePostDecorator($repository);
            }
        );
// add bindings


    }
}
