<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog__posts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your fields
            $table->integer('category_id')->unsigned()->nullable();
            $table->string('title');
            $table->string('url');
            $table->string('description')->nullable();
            $table->text('content')->nullable();
            $table->dateTime('publication_at')->nullable();

            $table->unique('url');
            $table->foreign('category_id')->references('id')->on('blog__categories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog__posts');
    }
}
