<?php

return [
    'field' => [
        'title' => 'Title',
        'url' => 'URL',
    ],

    'placeholder' => [
        'url' => '/category',
    ],

    'list resource' => 'List categories',
    'create resource' => 'Create categories',
    'edit resource' => 'Edit categories',
    'destroy resource' => 'Destroy categories',
    'title' => [
        'categories' => 'Categories',
        'create category' => 'Create a category',
        'edit category' => 'Edit a category',
    ],
    'button' => [
        'create category' => 'Create a category',
    ],
    'table' => [
        'id' => 'ID',
        'title' => 'Title',
        'url' => 'URL',
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
