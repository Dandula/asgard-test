<?php

return [
    'field' => [
        'category' => 'Category',
        'title' => 'Title',
        'url' => 'URL',
        'description' => 'Short description',
        'content' => 'Content',
        'publication at' => 'Publication at',
    ],

    'placeholder' => [
        'category' => 'General category',
        'url' => '/post',
    ],

    'list resource' => 'List posts',
    'create resource' => 'Create posts',
    'edit resource' => 'Edit posts',
    'destroy resource' => 'Destroy posts',
    'title' => [
        'posts' => 'Posts',
        'create post' => 'Create a post',
        'edit post' => 'Edit a post',
    ],
    'button' => [
        'create post' => 'Create a post',
    ],
    'table' => [
        'id' => 'ID',
        'category' => 'Category',
        'title' => 'Title',
        'url' => 'URL',
        'short description' => 'Short description',
        'content' => 'Content',
        'publication at' => 'Publication at',
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
