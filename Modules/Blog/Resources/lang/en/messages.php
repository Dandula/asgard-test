<?php

return [
    'title is required' => 'The title is required.',
    'url is required' => 'The URL is required.',
    'url must be unique' => 'The URL must be unique.',
];
