<div class="box-body">
    {!! Form::normalInput('title', trans('blog::posts.field.title'), $errors) !!}
    {!! Form::normalInput('url', trans('blog::posts.field.url'), $errors, null, ['placeholder' => trans('blog::posts.placeholder.url')]) !!}
    {!! Form::normalSelect('category_id', trans('blog::posts.field.category'), $errors, $categories_choise, null, ['placeholder' => trans('blog::posts.placeholder.category')]) !!}

    <div class='form-group{{ $errors->has('description') ? ' has-error' : '' }}'>
        {!! Form::label('description', trans('blog::posts.field.description')) !!}
        <textarea class="form-control" name="description" rows="3" cols="80" placeholder="{{ trans('blog::posts.field.description') }}">{{ old('description') }}</textarea>
        {!! $errors->first("description", '<span class="help-block">:message</span>') !!}
    </div>

    {!! Form::normalInputOfType('datetime-local', 'publication_at', trans('blog::posts.field.publication at'), $errors) !!}

    @editor('content', trans('blog::posts.field.content'), old('content'))
</div>
