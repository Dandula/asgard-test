<div class="box-body">
    {!! Form::normalInput('title', trans('blog::categories.field.title'), $errors) !!}
    {!! Form::normalInput('url', trans('blog::categories.field.url'), $errors, null, ['placeholder' => trans('blog::categories.placeholder.url')]) !!}
</div>
