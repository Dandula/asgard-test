<div class="box-body">
    {!! Form::normalInput('title', trans('blog::categories.field.title'), $errors, $category) !!}
    {!! Form::normalInput('url', trans('blog::categories.field.url'), $errors, $category, ['placeholder' => trans('blog::categories.placeholder.url')]) !!}
</div>
