<?php

namespace Modules\Blog\Composers;

use Illuminate\Contracts\View\View;
use Modules\Blog\Repositories\PostRepository;

class AllPostsComposer
{
    private $posts;

    public function __construct(PostRepository $posts)
    {
        $this->posts = $posts;
    }

    public function compose(View $view)
    {
        $view->with('posts', $this->posts->allPosts());
    }
}