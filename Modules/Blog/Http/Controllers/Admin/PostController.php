<?php

namespace Modules\Blog\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Blog\Entities\Category;
use Modules\Blog\Entities\Post;
use Modules\Blog\Http\Requests\CreatePostRequest;
use Modules\Blog\Http\Requests\UpdatePostRequest;
use Modules\Blog\Repositories\PostRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class PostController extends AdminBaseController
{
    /**
     * @var PostRepository
     */
    private $post;

    public function __construct(PostRepository $post)
    {
        parent::__construct();

        $this->post = $post;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $posts = $this->post->all();

        foreach ($posts as $post) {
            $category = Category::find($post->category_id);
            if ($category)
                $post->category_name = $category->title;
        }

        return view('blog::admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::all();

        $categories_choise = [];
        foreach ($categories as $category) {
            $categories_choise[$category->id] = $category->title;
        }

        return view('blog::admin.posts.create', compact('categories_choise'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePostRequest $request
     * @return Response
     */
    public function store(CreatePostRequest $request)
    {
        $this->post->create($request->all());

        return redirect()->route('admin.blog.post.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('blog::posts.title.posts')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Post $post
     * @return Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();

        $categories_choise = [];
        foreach ($categories as $category) {
            $categories_choise[$category->id] = $category->title;
        }

        if ($post->publication_at)
            $post->publication_at = Carbon::createFromFormat('Y-m-d H:i:s', $post->publication_at)->format('Y-m-d\TH:i:s');

        return view('blog::admin.posts.edit', compact('post', 'categories_choise'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Post $post
     * @param  UpdatePostRequest $request
     * @return Response
     */
    public function update(Post $post, UpdatePostRequest $request)
    {
        $this->post->update($post, $request->all());

        return redirect()->route('admin.blog.post.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('blog::posts.title.posts')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Post $post
     * @return Response
     */
    public function destroy(Post $post)
    {
        $this->post->destroy($post);

        return redirect()->route('admin.blog.post.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('blog::posts.title.posts')]));
    }
}
