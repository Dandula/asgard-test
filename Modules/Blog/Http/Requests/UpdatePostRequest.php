<?php

namespace Modules\Blog\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdatePostRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'title' => 'required',
            'url' => 'required',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'title.required' => trans('blog::messages.title is required'),
            'url.required' => trans('blog::messages.url is required'),
        ];
    }

    public function translationMessages()
    {
        return [];
    }
}
