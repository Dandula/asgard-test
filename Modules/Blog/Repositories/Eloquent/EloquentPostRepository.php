<?php

namespace Modules\Blog\Repositories\Eloquent;

use Modules\Blog\Repositories\PostRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPostRepository extends EloquentBaseRepository implements PostRepository
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function allPosts()
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }
}
