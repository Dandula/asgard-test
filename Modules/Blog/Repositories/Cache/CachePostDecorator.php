<?php

namespace Modules\Blog\Repositories\Cache;

use Modules\Blog\Repositories\PostRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePostDecorator extends BaseCacheDecorator implements PostRepository
{
    public function __construct(PostRepository $post)
    {
        parent::__construct();
        $this->entityName = 'blog.posts';
        $this->repository = $post;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function allPosts()
    {
        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember(
                "{$this->locale}.{$this->entityName}.allPosts",
                $this->cacheTime,
                function () {
                    return $this->repository->allPosts();
                }
            );
    }
}
